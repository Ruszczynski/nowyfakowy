<!doctype html>
<html lang="pl">

<head>
    <meta charset="utf-8">

    <title>The HTML5 Herald</title>
    <meta name="bo sie uczę" content="Rowerki Emilka">
    <meta name="byczek Emil" content="testowa">

    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/style.css">

</head>

<body>
    <div id="header">
        <div id="banner">
            <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Równica.jpg" />
            <div id="info-container">
                <div id="info">
                    <h1><?php bloginfo('name');?></h1>
                    <h3><?php bloginfo( 'description');?></h3>
                </div>
            </div>
        </div>
        <div id="menu">
					<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) );?>
        </div>
    </div>